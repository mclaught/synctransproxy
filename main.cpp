/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: user
 *
 * Created on 10 сентября 2020 г., 12:07
 */

#include <cstdlib>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <iostream>
#include <arpa/inet.h>
#include <string.h>
#include <unistd.h>
#include <map>

using namespace std;

int sock;
map<uint16_t, sockaddr_in> addrs;

void process(void* buf, int sz, sockaddr_in* addr){
    uint16_t id = *reinterpret_cast<uint16_t*>(buf);
    uint16_t cmd = id & 0x8000;
    id &= 0x7FFF;
    if(cmd == 0){
        cout << "OUT: " << inet_ntoa(addr->sin_addr) << ":" << ntohs(addr->sin_port) << endl;
        addrs[id] = *addr;
    }else{
        cout << "IN: " << inet_ntoa(addr->sin_addr) << ":" << ntohs(addr->sin_port) << endl;
        
        *reinterpret_cast<uint16_t*>(buf) = 0xAA55;
        
        sockaddr_in to = addrs[id];
        if(sendto(sock, buf, sz, 0, reinterpret_cast<sockaddr*>(&to), sizeof(sockaddr_in)) == -1){
            cerr << "sendto: " << strerror(errno) << endl;
        }
    }
}

/*
 * 
 */
int main(int argc, char** argv) {
    sock = socket(AF_INET, SOCK_DGRAM, 0);
    if(sock == -1){
        cerr << "socket:" << strerror(errno) << endl;
        return 1;
    }
    
    sockaddr_in addr;
    memset(&addr, 0, sizeof(sockaddr_in));
    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr = INADDR_ANY;
    addr.sin_port = htons(40471);
    if(bind(sock, reinterpret_cast<sockaddr*>(&addr), sizeof(sockaddr_in)) == -1){
        cerr << "bind:" << strerror(errno) << endl;
    }
    
    fd_set rd_set;
    timeval tv = {1,0};
    char buf[1500];
    
    while(true){
        FD_ZERO(&rd_set);
        FD_SET(sock, &rd_set);
        
        int cnt = select(sock+1, &rd_set, nullptr, nullptr, &tv);
        if(cnt < 0){
            cerr << "select:" << strerror(errno) << endl;
        }else if(cnt > 0){
            sockaddr_in from;
            memset(&from, 0, sizeof(sockaddr_in));
            from.sin_family = AF_INET;
            
            socklen_t len = sizeof(sockaddr_in);
            
            int rd = recvfrom(sock, buf, 1500, 0, reinterpret_cast<sockaddr*>(&from), &len);
            if(rd > 0){
                process(buf, rd, &from);
            }
        }
    }

    return 0;
}

