#!/bin/sh
### BEGIN INIT INFO
# Provides:          synctrproxy
# Required-Start:    $local_fs $network
# Required-Stop:     $local_fs $network
# Default-Start:     3 4 5
# Default-Stop:      0 1 6
# Short-Description: Synctrans Proxy Server
# Description:       Synctrans Proxy Server
### END INIT INFO

# Author: mclaught <sergey@taldykin.com>
#

DESC="Synctrans Proxy Server"
NAME=synctrproxy
DAEMON=/usr/sbin/${NAME}

case $1 in
    start|restart|force-reload)
        echo "Starting ${DESC}..."

        cd /
        exec > /dev/null
        exec 2> /dev/null
        exec < /dev/null 

        pidof ${NAME}
        if [ $? -eq 0 ]
        then
            killall ${NAME} -s 9
        fi

        .${DAEMON} &
        ;;

    stop)
        echo "Stoping ${DESC}..."

        pidof ${NAME}
        if [ $? -eq 0 ]
        then
            killall ${NAME} -s 9
        fi
        ;;

    status)
        pidof ${NAME}
        if [ $? -eq 0 ]
        then
            echo "Running"
            exit 0
        else
            echo "Stoped"
            exit 1
        fi         
	;;
esac


