#!/bin/bash

apt update
apt install gcc g++ make gdb git

git clone https://gitlab.com/mclaught/synctransproxy.git
cd synctransproxy

make CONF=Release
make install
